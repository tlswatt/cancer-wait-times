# Cancer wait times

Download the cancer wait times data, clean and tidy and analysis of 2020 compared to previous years.

I don't yet know how to web scrape in R, so first step is to go here to download the data:
https://www.england.nhs.uk/statistics/statistical-work-areas/cancer-waiting-times/

The relevant spreadsheet is the Nation Time Series with Revisions which is updated monthly. 

Download this before you start. 

